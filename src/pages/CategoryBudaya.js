import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const CategoryPolitik = () => {
  const [colorBudaya, setcolorBudaya] = useState("orange");
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar colorBudaya={colorBudaya} />
      <CardNews1 />
      <div className="news-content">
        <HighlightNews />
        <NewsList />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 />
      </div>
      <Footer />
    </div>
  );
};

export default CategoryPolitik;
