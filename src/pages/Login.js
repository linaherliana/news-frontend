import React, { useState } from "react";
import { Link } from "react-router-dom";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Button, Form, Input } from "antd";
import "../App.css";

import { useNavigate } from "react-router-dom";
import axios from "axios";
import useAuth from "../hooks/useAuth";
import Snackbar from "../components/Snackbar";

function Login() {
  const { setToken } = useAuth();
  const navigate = useNavigate();
  const [error, setError] = useState("");
  const [snackType, setSnackType] = useState("");
  const handleSubmit = (data) => {
    axios
      .post(`${process.env.REACT_APP_SITE}/auth/login`, data)
      .then((res) => {
        setToken(res.data.accessToken);
        navigate("/", { replace: true });
      })
      .catch((err) => {
        setError(err.response.data.error);
        setSnackType("error");
      });
  };

  return (
    <div className="container">
      <div className="form-box">
        <h2 style={{ textAlign: "center" }}>Login</h2>
        {error && (
          <div style={{ marginBottom: "2rem" }}>
            <Snackbar message={error} type={snackType} />
          </div>
        )}
        <Form onFinish={handleSubmit}>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Username harus diisi!",
              },
            ]}
          >
            <Input prefix={<UserOutlined />} placeholder="Username" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Password harus diisi!",
              },
            ]}
          >
            <Input.Password prefix={<LockOutlined />} placeholder="Password" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              Login
            </Button>
          </Form.Item>
        </Form>
        <p>
          Belum punya akun? <Link to="/register">Daftar di sini</Link>
        </p>
      </div>
    </div>
  );
}

export default Login;
