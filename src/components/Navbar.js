import React from "react";
import { Link } from "react-router-dom";
import { Input, Space } from "antd";

const Navbar = (props) => {
  const { Search } = Input;
  const colorPolitik = props.colorPolitik;
  const colorOlahraga = props.colorOlahraga;
  const colorEkonomi = props.colorEkonomi;
  const colorEntertainment = props.colorEntertainment;
  const colorKpop = props.colorKpop;
  const colorBudaya = props.colorBudaya;
  const colorMancanegara = props.colorMancanegara;

  return (
    <div className="navbar">
      <section id="header-navbar">
        <div class="header">
          <div>
            <Link to={`/`}>
              <img
                class="logo"
                src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIHBhUUBxMWFhUXGRoYGBgYFxcZGhYZFxkdHRcXFxcaICggGB4lGxYYITEiJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGy0mICYtLS0tLS0tLy0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAIcBdQMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABQYDBAcCAQj/xABLEAABAgMFAwQNCAgGAwAAAAABAAIDBBEFBhIhMUFRYSJxgbEHExQWMjZSU3KCkaHSFSNCkqKywdE1YoOTo7PC4SYzNENUcyTw8f/EABoBAQADAQEBAAAAAAAAAAAAAAACBAUDAQb/xAA1EQABAwIDBQcDBAIDAQAAAAABAAIDBBEhMVESQWFx0QUTIpGhwfAUgbEykuHxQlIzYoIV/9oADAMBAAIRAxEAPwDuKIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiLFFiNhMrFIA3lYWx3Rf8ptB5TsvY3U+5EW2iw8v9X3hecZa4CJQV0ofwIRFsIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIixvcGMq7IDMqnxb/ADMXzUFxHFwH4FSt757uOxHBnhP5A5j4X2QfaFzIsWR2hXPheGR6XPt84rY7Noo5WF8gvjYe/wA4K5O7IXkS/ti0/oK2rIvNHtqLSRlqNBo6I6IcDenByjwHCtNVB3buu61KRJ2rYOoGjonMfot46nZvXRJaXZKwAyXaGtaKAAUAHMutH9VIA+R1hpYXPph+feFYaSLwRMBdrc2HrifnLHDlKOxRzidvOg9Fuzr4r7OzsORly+ce1jRtcaD+54KDvFeiHZYLJWkSN5NeSz0yNv6oz5q1XPLRmYlpTHbJ5xe7ZXRo3Nbo0f8Apqp1VfHD4RidOp9lzpOzXz+J2Ddd55D3Vntm/wCS7DY7MvLiA5+izI9LvYoyBfaYgkljIJcdXFryfaX+5QHaE7Ssh3aEzjfatyW9H2dSsFtgHicT85WVlkuyBFZPs+UGs7WTR2FpBaD9IZmtDQ03VXSoUURWAsIIIqCMwQdCCvz/ADx/8im5X/sbXj7YzuWdOYBMInaBmWdGo4VGwLRoatzjsSG98j7LP7ToGBnexNtbMDTX7b+HJdHREWqsBERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERa8xHECA57tGgk9AqvCbIqPfaa7faIY05QxT1nZn3YfevN17vfKDhFnh82DyWn/cI2n9QbtvNrjsyQdbtrO7p8AHHEO/EahoPHPmAPBdBhQxCYAwAACgAyAA0ACxaOn+qlNTIMCcBywBPIAffktepqPp4xTxnEDE/NfxzWTRUm8V6DEcYVkupsMQanhD+L2b1IzfbLwPLJMlkuDR8TbFI1bD3t2YtDxAoZGQs6XsiXrBa1oA5T3UqRvc47Pcr83ezDZjOyN7t55db8tVSiMcJ2njaO4bhz6ea5y2xo5byYEWnoP/LNYYkqYT6RWlp3OBB9hXRu+WU7oDDGFSaA4XYa+nTD01ot+dkoc/BwzLQ4e8HeDsKzj2TG5p7qTEciOWGXtotD/wCq9p8bOq5P2hHQcCm7Rs4yM45js6Zg72nQ/hzgqKtf5qQdx5I6dfdVYZDmvLDmDYrUZMJLbO9VSKcbyd5qkGI6BFDoRLXNIc0jUEGoI6V6cF4or4V8hdpunbrbdsoOyERtGxG7nU1A8k6jpGxWFcMuxbL7DtUPFS08l7fKadekaj+5XapaYbMy7XwCHNcAWkbQdFv0lR3zMcxn1+b7r5LtGj+nku39Jy4ajpw43WyiIraz0RERERERFVb02/FsibY2XDCC2pxAnOpGwhTdkTDpqy4b41MTmgmmlTuVQ7If6Rh+h/UVa7ufoGD6AVCGV7qmRpOAyHktWogjbRRPAFycTvOak0RFfWUqXb95Y1m2mYcFsPCACMTSTmAdQ4K2yzzEl2l2pAPtC51fXxgfzM6guiSf+lZ6LepUKWR7ppWuOAOHDErVroWMp4XNABIx44DqVULevNGs61nwoDYdG4aYmuJzaHGpDhtJXnvmn/8Ajj91F/NRF9PGOL6v8tqvHfJKefb7Hfkq0b3ySyAyFtibZakellcfHHDBE5sIcXNF8OA0G+6rxvRPf8dv7uJ8SyWDeuJO2g2FONZR+QLQRQ0JFQSag0op3vklPPN+1+So1iPEW9EMw9C/EOYuJ6l7JI+N7NmXaubEYcF4yOOeOTbg2LAkHEbjwGVlbL2W1Esgwu5Qw48dcQJ8HDSlCPKK3bvTz7RstsSYpiJcDhBAycRoSdyg+yN4ED9p/SpS5Pi8znPWu8cjzWOZfC2X7VTlhjHZ7JABtE57/wDLorAqxeu3IlkRIfcwYcQdXECdC2lKEb1Z1RuyL/nQfRf1hda17mQFzTY4fkLj2bG2Spa14uMcDyKkn2xG72Gx4TWmITQgNcRTGW+CDXQb1F99k55hv7uJ8SsN0fFyF6333KaUGxSSsY8SEXaN18bZrqZ4YJHsMQd4neV7AZHJc9dfSZa6j2Qgdxa7qxLP32TnmGfu4nxKIvH4yxPTH4LqSrUwmlc9pkI2Tb89Feqn08DI3dy07QvywB04qu3YtSNafbO72huHDho1za1xV1JroFNTEZsvAc+MaNaCSdwAzWwqVfu1OQIEI60dE5tWt/HoG9XJH/TQlzjcjXecbfOF1lxxfV1IaxuyDuG4AY/NTZaL76xxEJhth4amlWurSuQJDtabVd7PnGz0m2JB8FwrzbweINR0Kstux/hYtw/Pn5zjiAyZ9XLnNVp3GtTtMyYMY5PzZXY+mY6QPaOKrQyyxSNbMb7QwywOnseYV6pggmhc+mFiw423jX3HIq/Ko3nvBFsi0Gtl2sLSwOOIEmpc4ahwyo0K3LnvZC/S7P8Arb956sV8jo4S5psVV7LhZLUbLxcWOatViW1DteD83yXjwmHUcRvHFTC5jaNlxbFiMiyrjgNCxw1aSPBduOdNx9ytN3byMtNohzNGxfc/i3ceHs4Qp6sl3dTYO9D89dylV0Aa3voDdnqPnmN+q93rtiJZMGGZUNJcSDiBOg2UIW5d+bdaFkMiTNMTsVcIIGTiBkSdgUL2Q/8ARwvSd1KVud4tQvX++5GyPNY5hOGzlx8KSQsHZ7JABtbWfDxdAptVu+U52izg0avOg1o3OnSaKyKnzL/lC+ENurWEn90Cf5hClXEmMRjN52fPP0uqlMLP2zk3Hp6qasGz/kyzWsd4Z5TzvcdegZDmAW3OwO6YWFxOE+EB9IeTXYDt4LbULblrCQhUh0MR3gjYB5TuHDb7adnmOCLxYNA9PnquYD5H4Ykr1aVqw7LghtKupyWNoMhkPRbx9lVUZ+biWlFrOOqNjBk1vMNp4nNYjV8QuiEucTUuOpKyNavl6ztGSoNhg3Tr0yWtBTNhF8zr065rVnIY7kdjGVD1LpMm0slGCJqGtB56CqptjyXyhaAFOQwh7zvINWs6SK8wO9XpanYkLmxuef8AK1vtf3NvsqddJtOA0VVvYwd2MO3D1E06yqLeV/LYznceofirlbcft1ouw6No0erWvvJHQqHbMXui0HnYDhHq5ddVl1Lg+se4a/gALW7LafDfcPyoh4WNZ3heKKQW2QvjQug9jS03ue6XiVLMJe0+TmA4cxLq89d6oTQuh9i+TpBixTtLWDoGJ33m+xXKHa79tvvyVDtMN+kdtcLc7iyv6Ii+hXx6IiIiIiIioPZD/SMP0P6irXdz9BQfQCiL0WBFtecY6XLAGtpyiRnUnY0qI7yZny4X13/Asq0sU73tYSDxtpzW8DBPSRxukDSPvryXQUXP+8mZ85C+u/4FJ3bu/GsqfLphzHNLC3kucTUlp0LRuKssqJnOAdEQNb5eipS0dOxhc2YE7hbPhmoC+vjA/mZ1BdEk/wDSs9FvUqnb92Ytp2kYkF0MNIAGJzgchTQNKtssww5dodqAB7AoUsb2zSucLAnDjiV0rp4308LWm5Ax4YDoVza+R/xHF9T7jVae8mXH0o31mfCtG3bsRrRtR8SC6GAcNMTnA5MDTUBp2hXQLlT0oMspkbgThfm44ei7VNeWwQiF+IbY25Nz9VWe8mW8qL9Znwrasq7kCzY+OEHF4rRzjWldaAADpop1FdbSwtNw0XWc+tqHjZc82VL7I3gQP2n9Klbkj/DrOc/eX28ti/K8q0QnBr2ElpOmYzBppsz4Kud5MzsfC+u/4FSe2WOpdK1m0CNbadNFoxOgno2wvkDSDfH/ANctdV0FUbsi/wCdB9F/WFg7yZnzkL67/gXzvJmCc4kKnpPPuwryoknmjLO6Iy36EHRTpYKanlEvfA2vha2YtqVZ7o+LkL1vvuU0tGzJQSEiyGw1DRSu87TTiareWjCwsja07gB5Cyxp3iSV7xkST5m65bePxliemPwXUlSbTutGnbWfFhuh4XOBFXOBpQbA3grsqdFE9j5S4WucPM9VodozRyRQhhvYY8MG9Fo2nOts6QdEi6NGm86Bo5zQLmMONFmbRMZrC92PGeS5wrWoBA2cOCu15rIj2qWNlnQxDbmQ5zgS7SuTTkB1lSFgWcLKsxsMULtXkbXHXoGQ5gFGeKSeYDENbjfU8Pm5e0tRFSwbWDnu3aDj7/ZVjvkn/NfwnqvThiidMWMwscXYgcLmjFWtW145rryirds4WpZrmGgdqwnY4adGo5iV5NRPc3/kJO6+q6U/akTHi0QaDgSNPJfLCtIWrZjXjwvBeNzhr0HI8xVT7IX6XZ/1t+89St27DmLGmyYjoZhuFHAOcTUaOALRns5il57vRbYtAPlzDAawN5RINQ5x2NOXKC8nbLNS7JadrD+17Sugp60lrhsEGx3Dh83WVggwmxbPa2KAWloBBFQRQZEKk3juy6RrFkKlgzIzLmca6lo36j3q9yze1wGh2oAB6AthXJ6ZkzbOz10WdS1clNIXMyOY3HoeP5XKrRtt9pWcyHN5uY6of5TSKUdx0z29d6ud4tQvX++5RNuXS7pi47LLWEnlNdUN520BpzUpzbZuwJN1nWQyHMUxNxVwkkZuJFCQNhVOlhmZUEyf62vriLemuKv11TTyUobFh4r7OmDr8PLBbNqTPcki9+4Zc5yHvIVPuvEw3kbj+lDiNHPVruppUhfSd7WGQ2em7qb+PuVWZNGTmocWEKmG4OoNS3RwHO0kdKp1lUBWsvkzP75+33uoU9OTTOIzd7ZLqy59asUzFpxHO8otHM04R1E9KvUvGbMwGvgkFrgHNI2gioKpVpyESHaT+Q9wc5zmlrXOBDjWmQ1FadCs9ste6FoaCfFux3FV6AtDzfT+1pNC2ZOVfOzGCWGf0nbGDefwG32kbtn2BFms5r5tnRjPMNG9PsVok5VknADZcUHWd5O08VnUXZL5CHTCzdN56DnjyzXeoq2twZifT+V5s6SbISwZA0GZJ1cTqTxWG1p/uOVqPDdk0cd/MNfZvWzOTTJOAXzBoB7SdgA2lVCcmnTcYvjZbGt8lu7n4rVr6ttLFsM/URYDQZX5Ddx4XVKCIyuuct605iL3PLuduBPT/wDVTIuqs1vxsEmG+UfcM+uirL181ALBfU0LbMLtT+FrvCxgLK5fGhWgr6NC6/ciU7ku7Dxavq8+seT9kNXKJeCYkYNZqSGjnJoOtdwloAl5drIWjQGjmAoFp9ltu9ztBbz/AKWH25JaNkepJ8sPyVsIiLbXzaIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIioN67LmTOOiQoZisOyHm4AClCzU+rXoVUZakI5F1NhBBHQV2lQFt3XlbZqZllH+WyjX9J0d6wKyKnslsji9hxOOK16btINaGSjAbx06W5KrXXvGyzD2uZeDAJq0g1MIk5ggasJz4HgcugwIzY8IOguDmkVDgQQRvBGqrExHk7rBsOBBq/DUkNBdStKviOzJND/bJfe/eAPBhv8Asfmu0EzKdvdySAkeY4X32XOSjkqT3kMZseWPG38lW1RFoWzDkqhpxv8AJbqPSOjenPgVFC+8Hzb/AGt/Nb1n9zWxLiLCYBQkEEAUIGYeBkdaro+p70bNO5pdxv8Ac23rg+jkh8U7SAoGZnHzcXFMkcGjRo4D8ViqFJm+UvCyl4Ty0aEBjQeIFdF97+IPmn/Y/NYMlHE9xc+oBJ/6n87Q/jJaLYKm1mwm3MKk2/HxTmHyRTpOZ91FEvK6X37QfNP+x+a3LLZKWwBGgQW4mktNWtBDqDUDJxpShz1ViKgjdZscoJ5fyVbNbJTRjvYSAML3Bx8sLrkZPOvrAurNvFLzc6YEzDoC5zKvDS0kGlCOKp16rvGx53FLgmE88k64TrhJ6t45iozUoawvY7aANjhaysw122/u5GbDiLi5vccDYLxc2T7pvDCFMmnGfUFR9rCutqidj+zjCD48UUaW4Wk7RWrjzckCvAqYk71Qp20xBgMccRID8qGgJrTWmS0ezy2KEbRsXnDjuCxu0tueZ2wLhgx4Zk/NQrGiItRY6IiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIoS9E6+Qsdz5YgOq0AkA0qRXIqEuzb0a0bVEObcHNLXGmFozGhqAqz6pjJREb3P89FbjopJIXTAiwvzw+y0L6+Mg9Fn4q+dxw/Ns+qPyVDvtybwAu8lh6Kn8lau+qT89/DifCqlPJGyaXbIGO8ga6rQq4pZKeDu2k+HG1+Gi9WxZLZ6zXQ4IYwnDysIyoQTpwFFisqyPkeyIrMWOuJ1cOGnJApSp3LRtq88I2a75Li/OVbh5Lx9IYs3NppVZbtT0S0rCimadiILmg0AqMAOwAauXYSQPnGzi7ZzBuLY4Z+29VjDUx0x28G7QwIsd1jiMvvuUHcOG2JaLxFaD83tAP0m71tzl9ZKRmnw5iBExQ3OaaMhUq0kEir9MlGXQtCFZ849047CC2gOFxzqDo0HcpaM+x40Zz4zYZc4lziYcWpJNSfB3qpRyhtM0B7QbnMrQrYdqrc58b3Cwtsg58+Sz2rMQrRuc6PKQ8IeGkVa0OFIgGeGu7evXY9/R0T0x90LVte1ZNt3XQbMfTIBrAx4HhBxzc2g2lbfY9FLOif9n9I/NdmPDqthBB8O7K+N1XdG5lBIHNI8WF87YW/CqzrPfaVpTHaBVzTEfh8oB9CBxz9ysd3rTZbUmZe0+U+mVfptHHXGKa65V3rDdXxomP2n8xq+XrsR0ON3RZoNa1eG1q11fDbTPM602571XiY5jO+ZjmHDUX9vm+9upkZJL9M82NgWO0db3+bre7yWiY8dslZIzya7DkMv9sU0aAM+am9Rdgy3cd8Ww61wOe2ulaNdnRWS6tifJcv2yYHzrxn+q3yefaf7KEk/H79pE6nL2Vjy6OV+Ze3DQae5UIJI9iWGL9LWOx/2cRifYLoCIi2l86iIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiItOfkoc/LFk03E00qKkaGozGa1JKwZeQjY5SHhdQiuJ5yOuRJCl0XMxMLg4gXG+wuujZpGtLGuIBzFzZRdoWPAtEDuyGCRkDUg03VaQacFr96kn5n7cT4lOIvHwRPN3NBPEBTZUzMGyx5A0BIHlkoPvUk/M/wASJ8Sk5OUZJQAyWaGtGgC2UXrIY2G7WgcgAoyTyyCz3EjiSVBRLsSj3lzoVCdaOe0ewGgX3vUk/M/bifEpxFD6WE4ljf2jopisqALCR37j1UH3qSfmf4kT4lIyMnDkoIZKtDWjYN+8nUnnW2imyGNhuxoHIAKEk8sgs9xI4klRUpZEGSm3RJVpD3VqcTjXEanImgzC3IEu2XB7VWhcXEEk5uNTSugrU00zWyik1jW4NAHJQc97zdxJ5m6KKbYsBloduaw9sqTixO1IIPJrTQnYpVEcxrrbQvZGve2+ySL4GxzGh4IiIpKKIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIi/9k="
                width={50}
                height={50}
                alt="logo"
              />
            </Link>
          </div>
          <div className="search-bar">
            <Space>
              <Search
                placeholder="Search"
                enterButton
                style={{
                  width: 500,
                }}
              />
            </Space>
          </div>
          <ul>
            <button class="btn-login">
              <Link to={`/login`}>Login</Link>
            </button>
          </ul>
        </div>

        <div class="topnav">
          <nav>
            <Link to={`/politik`}>
              <h2 style={{ color: colorPolitik }}>Politik</h2>
            </Link>
            <Link>
              <h2 style={{ color: colorOlahraga }}>Olahraga</h2>
            </Link>
            <Link>
              <h2 style={{ color: colorEkonomi }}>Ekonomi</h2>
            </Link>
            <Link>
              <h2 style={{ color: colorEntertainment }}>Entertaiment</h2>
            </Link>
            <Link>
              <h2 style={{ color: colorKpop }}>Kpop</h2>
            </Link>
            <Link>
              <h2 style={{ color: colorBudaya }}>Budaya</h2>
            </Link>
            <Link>
              <h2 style={{ color: colorMancanegara }}>Manca Negara</h2>
            </Link>
          </nav>
        </div>
      </section>
    </div>
  );
};

export default Navbar;
