import { Layout } from "antd";
import Navbar from "../Navbar";
import { Outlet } from "react-router-dom";

const AppLayout = () => {
  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      <Layout>
        <Navbar />
        <Outlet />
      </Layout>
    </Layout>
  );
};

export default AppLayout;
