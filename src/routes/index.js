import { RouterProvider, createBrowserRouter } from "react-router-dom";
import ProtectedRoute from "./ProtectedRoute";
import UnProtectedRoute from "./UnProtectedRoute";

import Login from "../pages/Login";
import Home from "../pages/Home";
import Register from "../pages/Register";

const Routes = () => {
  // Define routes accessible only to authenticated users
  const routes = [
    {
      path: "/",
      element: <ProtectedRoute />, //RouteGuard
      children: [
        {
          path: "/",
          element: <Home />,
        },
      ],
    },
    {
      path: "/",
      element: <UnProtectedRoute />, // Wrap the component in ProtectedRoute
      children: [
        {
          path: "/login",
          element: <Login />,
        },
        {
          path: "/register",
          element: <Register />,
        },
      ],
    },
  ];

  // Combine and conditionally include routes based on authentication status
  const router = createBrowserRouter([...routes]);

  // Provide the router configuration using RouterProvider
  return <RouterProvider router={router} />;
};

export default Routes;
